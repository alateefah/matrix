package matrix.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;



import matrix.pojo.Matrix;

public class CsvReader 
{
    private String filePath;
    private List<String> fileContent = new ArrayList<String>();    
    private int[][] matrix;

    public CsvReader(String filePath)
    {
        this.filePath = filePath;
    }

    public List<String> getFileContent()
    {
        try {
            File file = new File(filePath);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                fileContent.add(data); 
            }
            scanner.close();
        }
        catch (FileNotFoundException e) 
        {
            throw new RuntimeException(e);
        }

        //set the size of matrix to be the number of rows in the file
        this.matrix = new int[fileContent.size()][fileContent.size()];

        return fileContent;
    }

    public Matrix transformFileContent() throws Exception
    {
        this.getFileContent();

        int rows = fileContent.size();
        
        for(int i = 0; i < rows; i++)
        {
            String[] rowContent = fileContent.get(i).split(",");
            
            //check that rows equal the columns. Throw exception otherwise
            if (rowContent.length != rows)
            {
                throw new Exception("Rows and Columns not equal.");
            }

            for (var x = 0; x < rowContent.length; x++)
            {
                try 
                {
                    matrix[i][x] = Integer.parseInt(rowContent[x]);
                } 
                catch (NumberFormatException e) 
                {
                    throw new NumberFormatException();
                }
                
            }    
        }

        return new Matrix(rows, rows, matrix);     
    }
}
