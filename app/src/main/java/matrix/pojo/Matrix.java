package matrix.pojo;

public class Matrix {
 
    private int rows;
    private int columns;
    private int[][] matrix;

    public Matrix(int rows, int columns, int[][] matrix)
    {
        this.rows = rows;
        this.columns = columns;
        this.matrix = matrix;
    }

    public void echo()
    {
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < columns; j++)
            {
                System.out.print(matrix[i][j]);
                if (j < columns-1) 
                {
                    System.out.print(",");
                }
            }  
            System.out.print("\n");          
        }
        System.out.println(); 
    } 

    public void invert()
    {
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < columns; j++)
            {
                System.out.print(matrix[j][i]);
                if (j < columns-1) 
                {
                    System.out.print(",");
                }
            }  
            System.out.print("\n");          
        }
        System.out.println(); 
    } 

    public void flatten()
    {
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < columns; j++)
            {
                System.out.print(matrix[i][j]);
                if (matrix[i][j] != matrix[rows-1][columns-1]) 
                {
                    System.out.print(",");
                }
            }           
        }        
        System.out.println("\n"); 
    } 

    public void sum()
    {
        int sum = 0;
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < columns; j++)
            {
                sum += matrix[i][j];
            }           
        }
        System.out.println(sum+ "\n"); 
    } 

    public void multiply()
    {
        int multiplication = 1;
        for (int i = 0; i < rows; i++) 
        {
            for (int j = 0; j < columns; j++)
            {
                multiplication *= matrix[i][j];
            }           
        }
        System.out.println(multiplication+ "\n");
    } 
}
