package matrix;

import matrix.pojo.Matrix;
import matrix.util.CsvReader;

public class App {

    public static void main(String[] args) throws Exception {

        if (0 < args.length) {
            String fileName = args[0];

            if (!fileName.endsWith(".csv") && !fileName.endsWith(".CSV")) 
            {
                throw new Exception("Only CSV file allowed.");
            }
            
            CsvReader csvReader = new CsvReader(fileName);
            Matrix matrix = csvReader.transformFileContent();
            
            System.out.println("Echo:");
            matrix.echo();

            System.out.println("Invert:");
            matrix.invert();

            System.out.println("Flatten:");
            matrix.flatten();

            System.out.println("Sum:");
            matrix.sum();

            System.out.println("Multiply:");
            matrix.multiply();    
        } 
        else 
        {
            throw new Exception("Please specify file name.");
        }  
    }
}
