package matrix;

import java.io.File;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class AppTest 
{
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
    @Test(expected = Exception.class)
    public void callMainFunction_noCsvFileInput_throwsException() throws Exception {              
        App.main(new String[] {""});
    }

    @Test
    public void callMainFunction_ValidCsvFileInput_noException() throws Exception {           
        File file = folder.newFile("matrix.csv");   
        App.main(new String[] {file.getAbsolutePath()});
    }
 
    @Test(expected = Exception.class)
    public void callMainFunction_invalidFileType_throwException() throws Exception {    
        File file = folder.newFile("matrix.txt");          
        App.main(new String[] {file.getAbsolutePath()});
    }
}
