package matrix.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MatrixTests 
{
    int[][] input = {{1,2,3}, {4,5,6}, {7,8,9}};
    private Matrix matrix = new Matrix(3, 3, input);
    
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void echoMatrixTest() 
    {
        String expected = "1,2,3\n4,5,6\n7,8,9\n";
        matrix.echo();        
        assertEquals(expected.trim(), outputStreamCaptor.toString().trim()); 
    }
    
    @Test
    public void invertMatrixTest() 
    {
        String expected = "1,4,7\n2,5,8\n3,6,9\n";
        matrix.invert();        
        assertEquals(expected.trim(), outputStreamCaptor.toString().trim()); 
    }

    @Test
    public void flattenMatrixTest() 
    {
        String expected = "1,2,3,4,5,6,7,8,9";
        matrix.flatten();        
        assertEquals(expected.trim(), outputStreamCaptor.toString().trim()); 
    }

    @Test
    public void sumMatrixTest() 
    {
        int correctSum = 45;
        int incorrectSum = 50;
        
        matrix.sum();        
        assertEquals(correctSum, Integer.parseInt(outputStreamCaptor.toString().trim())); 
        assertNotEquals(incorrectSum, Integer.parseInt(outputStreamCaptor.toString().trim())); 
    }

    @Test
    public void multiplyMatrixTest() 
    {
        int correctMultiplication = 362880;
        int incorrectMultiplication = 50;
        
        matrix.multiply();        
        assertEquals(correctMultiplication, Integer.parseInt(outputStreamCaptor.toString().trim())); 
        assertNotEquals(incorrectMultiplication, Integer.parseInt(outputStreamCaptor.toString().trim())); 
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }
}
