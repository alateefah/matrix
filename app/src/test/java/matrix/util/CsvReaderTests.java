package matrix.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


public class CsvReaderTests 
{    
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
    @Test(expected = NumberFormatException.class)
    public void callCsvReader_addLettersToCsv_throwsNumberFormatException() throws Exception {  
        List<String> input = Arrays.asList("1,2,A", "4,5,6", "7,8,9");
        File file = writeToFile("matrix.csv", input);

        CsvReader csvReader = new CsvReader(file.getAbsolutePath());
        csvReader.transformFileContent();
    }

    @Test(expected = Exception.class)
    public void callCsvReader_unequalRowAndColumn_throwsNumberFormatException() throws Exception {  
        List<String> input = Arrays.asList("1,2,3", "4,5,6");
        File file = writeToFile("matrix.csv", input);

        CsvReader csvReader = new CsvReader(file.getAbsolutePath());
        csvReader.transformFileContent();
    }

    @Test
    public void callCsvReader_validInput_NoException() throws Exception {  
        List<String> input = Arrays.asList("1,2,3", "4,5,6", "7,8,9");
        File file = writeToFile("matrix.csv", input);

        CsvReader csvReader = new CsvReader(file.getAbsolutePath());
        csvReader.transformFileContent();
    }

    //create and write to temporary file
    public File writeToFile(String fileName, List<String> contents) throws IOException
    {
        File file = folder.newFile(fileName); 

        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferdWriter = new BufferedWriter(fileWriter);        
        for (String content : contents) {
            bufferdWriter.write(content+"\n");
        }
        bufferdWriter.close();
        
        return file;
    }
}
